/*
 * i18n.c: Internationalization
 *
 * VCD Player plugin for VDR (the Video Disk Recorder)
 * Copyright (C) 2002  Thomas Heiligenmann  <thomas@heiligenmann.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301 USA
 *
 * Italian translations provided by Gringo <vdr-italian@tiscali.it>
 *
 */


#include "i18n.h"

const tI18nPhrase Phrases[] = {
  { "VideoCD",
    "VideoCD",
    "",
    "VideoCD",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
  },
  { "Tracks",
    "Tracks",
    "",
    "Tracce",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
  },
  { "VideoCD SPI",
    "VideoCD SPI",
    "",
    "VideoCD SPI",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
  },
  { "SPI",
    "SPI",
    "",
    "SPI",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
  },
  { "No disc inserted",
    "Keine CD eingelegt",
    "",
    "Nessun disco inserito",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
  },
  { "No VideoCD detected",
    "Keine VideoCD erkannt",
    "",
    "Nessun VideoCD trovato",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
  },
  { "VCD",
    "VCD",
    "",
    "VCD",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
  },
  { "Setup.VCD$Drive speed",
    "Laufwerkgeschwindigkeit",
    "",
    "Velocit� lettore",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
  },
  { "Setup.VCD$Broken mode",
    "Nicht standardkonform",
    "",
    "Mod. non standard",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
  },
  { "Setup.VCD$Hide main menu entry",
    "Hauptmen�eintrag ausblenden",
    "",
    "Nascondi voce nel menu principale",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
  },
  { "Setup.VCD$Play tracks continuously",
    "Tracks nacheinander abspielen",
    "",
    "Riproduci tracce in modo continuo",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
  },
  { "Setup.VCD$Autostart replay",
    "Wiedergabe automatisch starten",
    "",
    "Riproduzione automatica all'avvio",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
  },
  { "Setup.VCD$Play sequence replay",
    "Wiedergabe nach Sequenz",
    "",
    "Riproduci in sequenza",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
  },
  { "Eject",
    "Auswerfen",
    "Izvrzi",
    "Eject",
    "Eject",
    "Ejectar",
    "Ejection",
    "Eject",
    "Avaa",
    "Wyrzucenie",
    "Expulsar",
    "Exsagogi",
    "Mata ut",
    "Ejecteaza",
    "Kidobni",
    "Expulsar",
  },
  { NULL }
};

